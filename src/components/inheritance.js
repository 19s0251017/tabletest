export default class Inheritance {
  constructor() {
    this.courseid = null;
    this.subjectAndTeacher = [
      {
        courseid: 1,
        subjectArray: [
          { subject: 'キャリアデザイン' },
          { subject: 'ネットワーク' },
          { subject: 'PBL' },
          { subject: 'ゼミ' },
          { subject: '作品制作' },
          { subject: 'PHP' },
          { subject: '国語表現' },
          { subject: 'ビジネスマナー' },
          { subject: 'ゲーム制作' },
        ],
      },
    ];
  }

  getId(coursesid) {
    return this.subjectAndTeacher[coursesid];
  }
}
